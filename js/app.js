'use strict';	
var myApp = angular.module('myApp', ['ngRoute', 'movieControllers']);

myApp.config(["$routeProvider", function($routeProvider){
	$routeProvider.
		when('/list',{
			templateUrl: 'partials/list.html',
			controller: 'listController'
		}).
		when('/gallery',{
			templateUrl: 'partials/gallery.html',
			controller: 'galleryController'
		}).
		when('/detail/:title',{
			templateUrl: 'partials/details.html',
			controller: 'detailController'
		}).
		otherwise({
			redirectTo: '/list'
		});
}]);
