'use strict';
var movieControllers=angular.module('movieControllers',[]);

movieControllers.controller('listController', ['$scope', '$http', function($scope, $http) {
	$scope.movieName="";
	$scope.data;
	$scope.options = [
    { label: 'title', value: 'title' },
    { label: 'rank', value: 'rank' }];
  	$scope.Selection = $scope.options[0];
	$scope.radio="ascending";


  $http.get('../data/imdb250.json').success(function(data) {
    $scope.data = data;
  }).error(function(data){
  	$scope.data=data||"error";
  });
  
  $scope.ascending=function(){
  	$scope.radio="ascending";
  	$scope.search();
  };

  $scope.descending=function(){
  	$scope.radio="descending";
  	$scope.search();
  };

  $scope.search = function(){
  	var flag=0;
  	var results = document.getElementById("results");
  	var ret="";
  	var filter=[];

  	for(var i=0; i<$scope.data.length; i++){
  		var name = $scope.data[i].title.toLowerCase();
  		if(name.search($scope.movieName.toLowerCase()) != -1 && $scope.movieName!=""){
  			filter.push($scope.data[i]);
  			flag=1;
  		}
  	}
  	if($scope.radio=="ascending"){
  		if($scope.Selection.value=="title"){
  			filter.sort(function(a,b){if(a.title<b.title) return -1; if(a.title>b.title) return 1; return 0;});
  		}
  		else{
  			filter.sort(function(a,b){if(a.rank<b.rank) return -1; if(a.rank>b.rank) return 1; return 0;});
  		}
  	}
  	else if($scope.radio=="descending"){
  		if($scope.Selection.value=="title"){
  			filter.sort(function(a,b){if(a.title<b.title) return 1; if(a.title>b.title) return -1; return 0;});
  		}
  		else{
  			filter.sort(function(a,b){if(a.rank<b.rank) return 1; if(a.rank>b.rank) return -1; return 0;});
  		}
  	}
  	else{

  	}
  	for(var i=0; i<filter.length; i++){
  		ret+="<div class='row panel'>";
      ret+="<img style='float:left;' height='50px' width='50px' src='./../data/images/" + filter[i].imdbID + ".jpg'>";
      ret+="<p> <em>Title</em>: <a href='#/detail/" + filter[i].title + "'>" + filter[i].title + "</a></p>";
      ret+="<p> <em>Rank:</em> " + filter[i].rank + "</p>";
      ret+="</div>";
  	}
  	if(flag==0 && $scope.movieName!=""){
  		ret="Not Found";
  	}
  	results.innerHTML=ret;
   };

   $scope.emit=function(){
   		document.getElementById("results").innerHTML="not send";
   		$scope.$emit('update_detail_controller', "$scope.message");
   };
}]);


movieControllers.controller('detailController',['$scope', '$routeParams', "$http",function($scope, $routeParams, $http){
		$scope.title=$routeParams.title;
		$scope.runtime="";
		$scope.genre="";
		$scope.director="";
		$scope.actors="";
		$scope.plot="";
		$scope.language="";
		$scope.country="";
		$scope.awards="";
		$scope.source="";
		$scope.votes="";
		$scope.rating="";
		$scope.nextTitle="";
		$scope.prevTitle="";

		$http.get('../data/imdb250.json').success(function(data) {
    		for(var i=0; i<data.length; i++){
				var movie = data[i];
				if(movie.title == $scope.title){
					var prev,next;
					if(i==0) {prev=data.length-1;next=i+1;}
					else if(i==data.length-1){next=0;prev=i-1;}
					else{next=i+1;prev=i-1;}
					$scope.prevTitle="#/detail/"+data[prev].title;
					$scope.nextTitle="#/detail/"+data[next].title;


					$scope.runtime = movie.runtime;
					for(var j=0; j<data[i].genre.length; j++){
						var genre=data[i].genre;
						$scope.genre += genre[j];
						if(j!=genre.length-1) $scope.genre += ", ";
					}

					for(var j=0; j<data[i].director.length; j++){
						var director=data[i].director;
						$scope.director += director[j];
						if(j!=director.length-1) $scope.director += ", ";
					}

					for(var j=0; j<data[i].actors.length; j++){
						var actors=data[i].actors;
						$scope.actors += actors[j];
						if(j!=actors.length-1) $scope.actors += ", ";
					}

					$scope.plot=data[i].plot;

					for(var j=0; j<data[i].language.length; j++){
						var language=data[i].language;
						$scope.language += language[j];
						if(j!=language.length-1) $scope.language += ", ";
					}

					for(var j=0; j<data[i].country.length; j++){
						var country=data[i].country;
						$scope.country += country[j];
						if(j!=country.length-1) $scope.country += ", ";
					}

					$scope.awards = data[i].awards;
					$scope.source = "./../data/images/" + data[i].imdbID + ".jpg";
					$scope.rating = data[i].imdbRating;
					$scope.votes = data[i].imdbVotes;

				}
			}
  		}).error(function(data){
  			$scope.data=data||"error";
  		});

	
}]);


movieControllers.controller("galleryController",['$scope','$http',function($scope, $http){
	$scope.genre="All";
	$scope.data;

	$http.get('../data/imdb250.json').success(function(data) {
    $scope.data = data;
    $scope.search();
  	}).error(function(data){
  	$scope.data=data||"error";
  	});

  	$scope.All=function(){
  		$scope.genre="All";
  		$scope.search();
  	};

  	$scope.Action=function(){
  		$scope.genre="Action";
  		$scope.search();
  	};

  	$scope.Adventure=function(){
  		$scope.genre="Adventure";
  		$scope.search();
  	};

  	$scope.Crime=function(){
  		$scope.genre="Crime";
  		$scope.search();
  	};

  	$scope.Comedy=function(){
  		$scope.genre="Comedy";
  		$scope.search();
  	};

  	$scope.Drama=function(){
  		$scope.genre="Drama";
  		$scope.search();
  	};

  	$scope.Musical=function(){
  		$scope.genre="Musical";
  		$scope.search();
  	};

  	$scope.Mystery=function(){
  		$scope.genre="Mystery";
  		$scope.search();
  	};

  	$scope.Romance=function(){
  		$scope.genre="Romance";
  		$scope.search();
  	};

  	$scope.Sci_Fi=function(){
  		$scope.genre="Sci-Fi";
  		$scope.search();
  	};

  	$scope.Thriller=function(){
  		$scope.genre="Thriller";
  		$scope.search();
  	};

  	$scope.Western=function(){
  		$scope.genre="Western";
  		$scope.search();
  	};

	$scope.search = function(){
  	var flag=0;
  	var results = document.getElementById("results");
  	var ret="";
  	var filter=[];
  	for(var i=0; i<$scope.data.length; i++){
  		var genre = $scope.data[i].genre;
  		if($scope.genre=="All"){
  			flag=1;
  			filter.push($scope.data[i]);
  		}

  		else{
  			for(var j=0; j<genre.length; j++){			
  				if(genre[j] == $scope.genre){
  					flag=1;
  					filter.push($scope.data[i]);
  				}
  			}
  		}
  	}
  	for(var i=0; i<filter.length; i++){
  		if(i%6==0) ret += "<div class='row'>";
  		ret += "<div class='small-2 large-2 columns' id='image'><a href='#/detail/"+ filter[i].title +"'><img src='./../data/images/" + filter[i].imdbID + ".jpg'></div>";
  		if(i==filter.length-1){
  			var offset=5-i%6;
  			for(var j=0;j<offset;j++){
  				ret += "<div class='small-2 large-2 columns'></div>";
  				i++;
  			}
  		}
  		if(i%6==5) ret +="</div>";
  	}

  	results.innerHTML = ret;
  	
   };	
}]);
