var left_arrow=document.getElementById("left_arrow");
var right_arrow=document.getElementById("right_arrow");
var currIndex = 1;


left_arrow.addEventListener("click", left, false);
right_arrow.addEventListener("click", right, false);
function left () {
	if(currIndex == 1) currIndex=4;
	else currIndex--;
	var imgPath = "img/image" + currIndex + ".jpeg";

	$('#image').fadeOut(500, function(){
		$('#image').attr("src", imgPath);
		$('#image').fadeIn(500);
		if(currIndex==1){
			$('#Home').css("background-color","rgba(100,100,100,0.9)");
		}
		else if(currIndex==2){
			$('#Home').css("background-color","rgba(200,199,136,0.9)");
		}
		else if(currIndex==3){
			$('#Home').css("background-color","rgba(210,210,210,0.9)");
		}
		else{
			$('#Home').css("background-color","rgba(247,208,190,0.9)");
		}
	});

}

function right(){
	if(currIndex == 4) currIndex=1;
	else currIndex++;

	var imgPath = "img/image" + currIndex + ".jpeg";

	$('#image').fadeOut(500, function(){
		$('#image').attr("src", imgPath);
		$('#image').fadeIn(500);

		if(currIndex==1){
			$('#Home').css("background-color","rgba(100,100,100,0.9)");
		}
		else if(currIndex==2){
			$('#Home').css("background-color","rgba(220,199,136,0.9)");
		}
		else if(currIndex==3){
			$('#Home').css("background-color","rgba(210,210,210,0.9)");
		}
		else{
			$('#Home').css("background-color","rgba(247,208,190,0.9)");
		}
	});
}

//this one is for modal
var project1 = document.getElementById("p1");
var close1= document.getElementById("close1");
project1.addEventListener("click", modal1, false);
close1.addEventListener("click",close1,false);
function modal1(){
	$("#projectOne").fadeIn();
	$("#blur").fadeIn();
}

$("#close1").click(function(){
	$("#projectOne").fadeOut(500,function(){
		$("#blur").fadeOut();
	});
});

var project2 = document.getElementById("p2");
var close2= document.getElementById("close2");
project2.addEventListener("click", modal2, false);
close2.addEventListener("click",closeTwo,false);
function modal2(){
	$("#projectTwo").fadeIn();
	$("#blur").fadeIn();

}
function closeTwo(){
	$("#projectTwo").fadeOut();
	$("#blur").fadeOut();
}

var project3 = document.getElementById("p3");
var close3 = document.getElementById("close3");
project3.addEventListener("click", modal3, false);
close3.addEventListener("click",closeThree,false);
function modal3(){
	$("#projectThree").fadeIn();
	$("#blur").fadeIn();
	
}
function closeThree(){
	$("#projectThree").fadeOut();
	$("#blur").fadeOut();
}

//change
var change = document.getElementById("change");

change.addEventListener("click",one,false);
function one(){
	document.getElementById("complain").innerHTML="I don't like programming with restrictions :(";
}

document.getElementById("play").addEventListener("click",play,false);
function play(){
	document.getElementById("audio").innerHTML="<audio autoplay loop src='img/flute.mp4' type=audio/mp4 />";
	document.getElementById("trick").innerHTML="Haha, you can't stop me :)";
}

